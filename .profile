#!/bin/bash
#******************************************************************************
# .profile -- initialize the environment at bash startup.
#
# This file is run to define some basic environment variables by bash
# and ZSH.
#******************************************************************************
# Set our umask for creating and opening files.

umask 022

# Initialize the path if it isn't.

[ -z "$PATH" ] && export PATH="/bin:/usr/bin:/sbin:/usr/sbin"

# Set the locale.

export LANG=fr_FR.UTF-8
export LANGUAGE=fr_FR:fr:en

# Some utilities run your editor for editing files on the run, such as
# `yay` for AUR packages editing (`PKGBUILD` file), `visudo` for the
# `/etc/sudoers` file, ...
#
# They know which editor to run using one of these environment variables;
# let's make them know nano is the best editor in console mode.

export VISUAL='nano'
export EDITOR='nano'

# Some other utilities like to have a pager, to display the content of
# a file in an interactive manner. Let's use `less` as our pager.

export PAGER="less"

# Tell applications (through the libc) to use /etc/localtime as the
# local timezone descriptor.

export TZ=":/etc/localtime"

# Okay, we're done for .profile.
