#!/bin/zsh
#******************************************************************************
# .zshenv -- initialize the environment at ZSH startup.
#
# This file is run first among the ZSH startup files.
# Read more: http://zsh.sourceforge.net/Doc/Release/Files.html
#******************************************************************************
# Set the $ZDOTDIR and disable global startup files.

export ZDOTDIR="$HOME"
setopt NO_GLOBAL_RCS

# Function to manage the dotfiles.

dotfiles() {
	git --git-dir="$HOME"/.dotfiles --work-tree="$HOME" "$@"
}

# Prepare the function to load scripts from a folder.
#
# Used to check if files are executable, doesn't anymore
# because you know, reality.

_load_scripts() {
	setopt +o NO_MATCH

	for src in "$@"; do
		if [ -f "$src" ]; then
			. "$src"
		elif [ -d "$src" ]; then
			for p in "$src/"*; do
				. "$p"
			done
		fi
	done

	setopt -o NO_MATCH
}

# Load the .paths.bash.

_load_scripts ~/.paths.bash

# First of all, we want to set up oh-my-zsh.
# If you don't know what it is, it's a collection of scripts made by the
# community around the Z shell.
#
# I also added my plugins, which are in my custom folder.
# And I disabled update prompt and auto upgrades, which can be done
# through `upgrade_oh_my_zsh`.

export ZSH="$HOME/.oh-my-zsh"
export ZSH_CUSTOM="$HOME/.zsh-custom"
export ZSH_THEME=evan-mod
export plugins=(git man cp sudo command-not-found)

[ -d "$ZSH" ] && DISABLE_UPDATE_PROMPT=true DISABLE_AUTO_UPDATE=true \
	source "$ZSH/oh-my-zsh.sh"

# Add the .bin and .man directories.

add_bin_path ~/.bin
add_bin_path ~/.local/bin
add_man_path ~/.man

# Define some more aliases I didn't know where to put.

alias rf='rm -rf'
alias l='ls -lah'
alias ll='ls -l'
alias ffmpeg='ffmpeg -loglevel error'
alias egrep='egrep --color=auto --exclude-dir={.bzr,CVS,.git,.hg,.svn}'
alias dd='\dd status=progress'

# Run a silent background script.
# Copied from http://terminallinux.org/p/76/

fart() {
	"$@" &>/dev/null &
}

# ---
# Run scripts.
# ---

# Finally, run the local environment script(s) if any.
# You won't find this file in the git repository, as it is
# more or less unique to each machine and set of needs.

_load_scripts "$HOME/.zshenv-local" "$HOME/.zshenv.local" \
	"$HOME/.zshenv.d"

# End of file.
