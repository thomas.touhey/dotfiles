"*******************************************************************************
" .vimrc -- run commands at VIM startup.
"
" Uses Vundle: https://github.com/VundleVim/Vundle.vim
"*******************************************************************************
" Require viM.
set nocompatible
filetype off

" Set the runtime path to include Vundle and initialize.
set rtp+="$HOME/.vim/bundle/Vundle.vim"
call vundle#begin("$HOME/.vim-bundle")

" Let Vundle manage Vundle.
Plugin 'VundleVim/Vundle.vim'

" Load additional plug-ins.
Plugin 'editorconfig/editorconfig-vim'

" Finish loading things.
call vundle#end()
filetype plugin indent on

" And set up some more things.
syntax on
set nu
set tabstop=4

" End of file.
