#!/bin/bash
#******************************************************************************
# .bashrc -- run commands at bash startup.
#
# This file is run to run commands & stuff.
#******************************************************************************
# Include our function defining functions to manage the path.

. "$HOME/.paths.bash"

# Okay, we're done for .bashrc!
