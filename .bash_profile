#!/bin/bash
#******************************************************************************
# .bash_profile -- executed when bash is run as a login shell.
#
# This file defines the basic environment variables, then acts upon
# the TTY the shell is on.
#******************************************************************************
# Load the main profile file.

. "$HOME"/.profile

# From here we want to check what to do next using the following variables:
# `$TTY` is the tty device name (could be `/dev/tty1` or `/dev/pts/0`).
# `$DISPLAY` is the X display number (e.g. `:0.0`).
# `$XDG_VTNR` is the virtual tty number (if set, only `7` for
#             `/dev/tty7` for example).

if [ -z "$DISPLAY" ] && [ -n "$XDG_VTNR" ] && [ "$XDG_VTNR" -eq 1 ]; then
	# We are on `/dev/tty1` and not in an X11 environment.
	# TODO: do we want to do that instead?

	startx
	logout
elif [ -z "$DISPLAY" ] && [ -n "$XDG_VTNR" ] && [ "$XDG_VTNR" -eq 8 ]; then
	htop
	logout
else
	which zsh 1>/dev/null 2>/dev/null \
		&& exec zsh
fi

# Used to contain logout from here.
# However we don't want to logout in case we haven't exec'd anything.
# So let's just keep on moving.

# End of file.
