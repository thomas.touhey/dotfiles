#!/bin/zsh
#******************************************************************************
# .zprofile — run event when the shell is not interactive.
#
# This file only loads local things.
#******************************************************************************
# Run the local login script(s) if any.
# You won't find this file in the git repository, as it is
# more or less unique to each machine and set of needs.

_load_scripts "$HOME/.zprofile-local" "$HOME/.zprofile.local" \
	"$HOME/.zprofile.d"

# End of file.
