#!/bin/zsh
# Evan theme from oh-my-zsh modified to display username instead of machine name,
#  and to use colors !
local ret_status="%(?:%{$fg_bold[green]%}➜ :%{$fg_bold[red]%}➜ %s)"

if [ $(id -u) -eq 0 ]; then
	PROMPT="%{$fg[red]%}idiot"
else
	PROMPT="%{$fg[green]%}%n"
fi
PROMPT="$PROMPT@%m %{$fg[cyan]%}:: %{$fg[yellow]%}%2~ %{$fg[cyan]%}%B»%b "
PROMPT2="%{$fg[yellow]%}%_ %{$fg[cyan]%}%B»%b "
