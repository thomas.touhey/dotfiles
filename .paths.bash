#!/bin/bash
# paths.bash -- paths variables management, for bash-compatible shells.
#
# Copyright (C) 2020 Thomas Touhey <thomas@touhey.fr>
# All rights reserved.
#
# Manages path with priorities.
# The principle behind every path is to have a sorted list of folders in
# which to look for elements. Examples of elements managed by this script
# are the following:
#
#  * the executables path (known as $PATH), where in the shell, when the
#    parent folder of a command isn't specified, the executable file is
#    looked for.
#  * the dynamic library path ($LD_LIBRARY_PATH) where, if defined,
#    dynamic libraries are looked for when running ELF executables.
#  * the manual path ($MANPATH) where, if defined, manpages are looked for.
#  * the include path for gcc ($C_INCLUDE_PATH) where, if defined,
#    C header files included with <> are looked for.
#  * the pkg-config path ($PKG_CONFIG_PATH) where, if defined, pkg-config
#    packages files are looked for.
#
# Dependencies are common Linux utilities:
#
#  * bash or compatible <https://www.gnu.org/software/bash/>
#  * gawk <https://www.gnu.org/software/gawk/>
#  * sed <https://www.gnu.org/software/sed/>
#  * realpath <https://www.gnu.org/software/coreutils/coreutils.html>

__thpath_gawk_cmd="$(command -v gawk 2>/dev/null)"
__thpath_sed_cmd="$(command -v sed 2>/dev/null)"
__thpath_sort_cmd="$(command -v sort 2>/dev/null)"
__thpath_realpath_cmd="$(command -v realpath 2>/dev/null)"
__thpath_manpath_cmd="$(command -v manpath 2>/dev/null)"

if [ -z "$__thpath_gawk_cmd" ]; then
	echo "gawk is required for this script to run." >&2
	return 1 2>/dev/null; exit 1
fi

if [ -z "$__thpath_sed_cmd" ]; then
	echo "sed is required for this script to run." >&2
	return 1 2>/dev/null; exit 1
fi

if [ -z "$__thpath_sort_cmd" ]; then
	echo "sort is required for this script to run." >&2
	return 1 2>/dev/null; exit 1
fi

if [ -z "$__thpath_realpath_cmd" ]; then
	echo "realpath is required for this script to run." >&2
	return 1 2>/dev/null; exit 1
fi

__thpath_gawk() {
	"$__thpath_gawk_cmd" "$@"
	return $?
}

__thpath_sed() {
	"$__thpath_sed_cmd" "$@"
	return $?
}

__thpath_sort() {
	"$__thpath_sort_cmd" "$@"
	return $?
}

__thpath_realpath() {
	"$__thpath_realpath_cmd" "$@"
	return $?
}

__thpath_manpath() {
	"$__thpath_manpath_cmd" "$@" 2>/dev/null
	return $?
}

# ---
# Main path utilities.
# ---
# Raw paths are made of <priority> and <path>.
# They are organized as sets of <priority>:<path>, separated by colons,
# e.g. "30:a:10:b:50:c", where priorities are not necessarily sorted.
#
# __thpath_lines(raw_path)
#
#   Produces lines on the output of the following form:
#
#     30:a
#     10:b
#     50:c
#
#   The result stays unsorted by priority, as it is not required by all
#   operations on raw paths.

__thpath_lines() {
	echo "$1" \
		| __thpath_gawk -F ':' '
			{ for (i = 1; i <= NF; i += 2) print $i":"$(i+1) }'
}

# __thpath_contains(var, path)
#
#   Check if the given path is already in the raw path or not.

__thpath_contains() {
	__thpath_lines "$(__thpath_indirect "$1")" | egrep :"$2"$ &>/dev/null
	return $?
}

# __thpath_sorted_lines()
#
#   Same as previous function, but the lines are sorted.

__thpath_sorted_lines() {
	__thpath_lines "$1" \
		| __thpath_sort -t: -nk1
}

# __thpath_as_raw()
#
#   Reads line from standard input, returns the variable as width.

__thpath_as_raw() {
	__thpath_gawk -F ':' '
		NR == 1 { printf $1":"$2 }
		NR > 1 { printf ":"$1":"$2 }'
}

# __thpath_without_line(raw_path, element)
#
#   Filter out the given element and produce as a series of lines.

__thpath_without_line() {
	__thpath_lines "$1" \
		| __thpath_gawk -F ':' -v NAME="$2" '
			{ if ($2 != NAME) print $1":"$2 }'
}

# __thpath_with_line(raw_path, element, priority)
#
#   Add a given value.

__thpath_with_line() {
	__thpath_without_line "$1" "$2"
	echo "$3:$2"
}

# __thpath_produce(raw_path)
#
#   Produces the final path out of the raw path, by sorting by priority
#   and returning the final form. For the example given previously,
#   the function outputs:
#
#     b:a:c

__thpath_produce() {
	__thpath_sorted_lines "$1" \
		| __thpath_gawk -F ':' '
			NR == 1 { printf $2 }
			NR > 1 { printf ":"$2 }'
}

# __thpath_without(raw_path, element)
#
#   Returns a raw path without the given element.

__thpath_without() {
	[ -z "$2" ] && return 1
	[[ "$2" == *:* ]] && return 1

	__thpath_without_line "$1" "$2" | __thpath_as_raw
}

# __thpath_with(raw_path, element, priority)
#
#   Returns a raw path with the given element at the given priority.

__thpath_with() {
	[ -z "$2" ] && return 1
	[ -z "$3" ] && return 1
	[[ "$2" == *:* ]] && return 1
	[[ "$3" == *:* ]] && return 1

	__thpath_with_line "$1" "$2" "$3" | __thpath_as_raw
}

# ---
# General commands.
# ---
# __thpath_indirect(var)
#
#   Get the indirection of the given variable name.

__thpath_indirect() {
	local varname="$1"

	if [ ! -z "$ZSH_NAME" ]; then
		echo "${(P)varname}"
	else
		echo "${!varname}"
	fi
}

# __thpath_colonful(var)
#
#   Check if the given string contains colons.

__thpath_colonful() {
	[[ "$1" == *":"* ]] || return 1
}

# __thpath_check(var)
#
#   Check that the variable is managed, call the initialize function
#   otherwise.

__thpath_check() {
	local varname="$1"
	local ivarname='__thpath_raw_'"$varname"

	if [ ! -v "$ivarname" ]; then
		if command -v "__thpath_init_$varname" 1>/dev/null 2>/dev/null; then
			declare -g "$ivarname="
			"__thpath_init_$varname"

			# In all cases, even if __thpath_add_path hasn't been called
			# at some point, we'll initialize the variable.

			declare -g "$varname=$(__thpath_produce "$(__thpath_indirect "$ivarname")")"
			export "$varname"
		fi
	else
		# For every element of the given final path, we want to see if it
		# is already in the raw path; if it is not, we add it in order not
		# to lose any manual modifications over the raw path.

		__thpath_add_paths "$1" "$(__thpath_indirect "$1")" 50 "y"
	fi
}

# __thpath_add_path_internal <varname> <force> <directory> <priority>
#
#   Add a directory to a variable using a given priority.

__thpath_add_path_internal() {
	local varname="$1"
	local force="$2"
	local dr="$3"
	local pr="$4"

	dr="$(__thpath_realpath -m -s "$dr")"
	if __thpath_colonful "$dr"; then
		echo "error: directory name should not contain colon" >&2
		return 1
	fi

	[ -z "$pr" ] && pr="50"

	if __thpath_colonful "$pr"; then
		echo "error: priority should not contain colon" >&2
		return 1
	fi

	# Compatibility check: let's see if the directory is a directory,
	# and if we have access to it.

	if [ x"$force" = x ]; then
		if [ ! -d "$dr" ]; then
			return 1
		fi
	fi

	# Effectively add the path.

	declare -g "$ivarname=$(__thpath_with "$(__thpath_indirect "$ivarname")" "$dr" "$pr")"
	declare -g "$varname=$(__thpath_produce "$(__thpath_indirect "$ivarname")")"
	export "$varname" "$ivarname"
}

# __thpath_add_path <cmd_name> <varname> [-f|--] <directory> <priority>
#
#   Add a directory to a variable using a given priority.

__thpath_add_path() {
	local dr=''
	local pr=''
	local cmdname=""
	local varname=""
	local forceopt=""
	local force=""
	local ivarname=''
	local lbound=3
	local rbound=4

	cmdname="$1"
	varname="$2"
	ivarname='__thpath_raw_'"$varname"

	forceopt="$3"
	dr="$3"
	pr="$4"
	if [ "$forceopt" = "--" ]; then
		dr="$4"
		pr="$5"
		lbound=4
		rbound=5
	elif [ "$forceopt" = "-f" ]; then
		force=y
		dr="$4"
		pr="$5"
		lbound=4
		rbound=5
	fi

	if [ "$#" -lt "$lbound" ] || [ "$#" -gt "$rbound" ]; then
		echo "usage: $cmdname [-f] path/to/directory [priority]" >&2
		return 0
	fi

	__thpath_check "$varname"
	__thpath_add_path_internal "$varname" "$force" "$dr" "$pr"
}

# __thpath_add_paths(var, directories_colon_joined, priority, if_not_exists)
#
#   Add a path to a variable using a given priority.
#   If `if_not_exists` is not none, we want to only add it if it doesn't exist.

__thpath_add_paths() {
	local i=''
	local p="$2"

	if [ ! -z "$ZSH_NAME" ]; then
		# Here, I cheat for a bit, because bash doesn't recognize the
		# syntax where zsh does.

		read -r -d '' dcod <<'EOF'
		IFS=:; for i ($=p); do
			if [ "$4" = "" ] || ! __thpath_contains "$1" "$i"; then
				__thpath_add_path_internal "$1" "y" "$i" "$3"
			fi
		done
EOF
		eval $dcod
	else
		for i in $p; do
			if [ "$4" = "" ] || ! __thpath_contains "$1" "$i"; then
				__thpath_add_path_internal "$1" "y" "$i" "$3"
			fi
		done
	fi
}

# __thpath_del_path(cmd_name, var, directory)
#
#   Remove a directory from a variable.

__thpath_del_path() {
	local dr=''
	local varname="$2"
	local ivarname='__thpath_raw_'"$varname"

	__thpath_check "$varname"

	if [ "$#" -lt 3 ] || [ "$#" -gt 3 ]; then
		echo "usage: $1 path/to/directory" >&2
		return 0
	fi

	dr="$(__thpath_realpath -m -s "$3")"
	if __thpath_colonful "$dr"; then
		echo "error: directory name should not contain colon" >&2
		return 1
	fi

	# Effectively add the path.

	declare -g "$ivarname=$(__thpath_without "$(__thpath_indirect "$ivarname")" "$dr")"
	declare -g "$varname=$(__thpath_produce "$(__thpath_indirect "$ivarname")")"
	export "$varname" "$ivarname"
}

# __thpath_print_path(cmd_name, var)
#
#   Print a given path.

__thpath_print_path() {
	local varname="$2"
	local ivarname='__thpath_raw_'"$varname"

	__thpath_check "$varname"

	if [ "$#" -lt 2 ] || [ "$#" -gt 2 ]; then
		echo "usage: $1" >&2
		return 0
	fi

	# Effectively add the path.

	__thpath_sorted_lines "$(__thpath_indirect "$ivarname")"
}

# ---
# Manage the $PATH
# ---
# __thpath_init_PATH()
#
#   Initialize the $PATH with the raw path available somewhere.
#
#   There is no command to give us an initial value for the $PATH
#   if it is initially empty, so in this case, we just add a few common
#   values, just in case.

__thpath_init_PATH() {
	local IFS=':'

	if [ -z "$PATH" ]; then
		__thpath_add_path "" "PATH" -- "/bin"
		__thpath_add_path "" "PATH" -- "/usr/bin"
	else
		__thpath_add_paths "PATH" "$PATH"
	fi
}

# add_bin_path(dir[, priority])
#
#   Add a directory to the $PATH.

add_bin_path() {
	__thpath_add_path "${FUNCNAME[0]}${funcstack[1]}" "PATH" "$@"
}

# add_path(dir[, priority])
#
#   Add a directory to the $PATH; compatibility function for old scripts.

add_path() {
	__thpath_add_path "${FUNCNAME[0]}${funcstack[1]}" "PATH" "$@"
}

# del_bin_path(dir)
#
#   Remove a directory from the $PATH.

del_bin_path() {
	__thpath_del_path "${FUNCNAME[0]}${funcstack[1]}" "PATH" "$@"
}

# del_path(dir)
#
#   Remove a directory from the $PATH; compatibility function for old scripts.

del_path() {
	__thpath_del_path "${FUNCNAME[0]}${funcstack[1]}" "PATH" "$@"
}

# print_bin_path()
#
#   Print the $PATH.

print_bin_path() {
	__thpath_print_path "${FUNCNAME[0]}${funcstack[1]}" "PATH" "$@"
}

# print_bin_path()
#
#   Print the $PATH; easier function.

print_path() {
	__thpath_print_path "${FUNCNAME[0]}${funcstack[1]}" "PATH" "$@"
}

# ---
# Manage the $LIBRARY_PATH
# ---
# __thpath_init_LIBRARY_PATH()
#
#   Initialize the LIBRARY_PATH with the raw path available somewhere.

__thpath_library_path_put_things() {
	local IFS='
'
	echo "$*" | __thpath_sort -r -V
}

__thpath_library_path_load_file() {
	local IFS='
'
	local line
	local _inc

	[ -f "$1" ] || return 1
	while IFS= read -r line; do
		# Remove the comments, forget about the empty lines.
		line="$(echo ${line%#*} | __thpath_sed 's/^[[:space:]]*//g' \
			| __thpath_sed 's/[[:space:]]*$//g')"
		[ -z "$line" ] && continue

		# Check if is an include.
		_inc="$(echo ${line#include} | __thpath_sed 's/^[[:space:]]*//g')"
		if [ ! x"$_inc" = x"$line" ]; then
			# Evaluate each configuration file.

			for i in $(eval "__thpath_library_path_put_things $_inc"); do
				__thpath_library_path_load_file "$i"
			done
			continue
		fi

		__thpath_add_path "" "LIBRARY_PATH" -f "$line"
	done < "$1"
}

__thpath_init_LIBRARY_PATH() {
	local LDLIB="$LD_LIBRARY_PATH"
	local CCLIB="$LIBRARY_PATH"
	local IFS=':'
	local i

	export LD_LIBRARY_PATH=
	export LIBRARY_PATH=

	# Gather existing paths.

	__thpath_add_paths "LIBRARY_PATH" "$LDLIB"
	__thpath_add_paths "LIBRARY_PATH" "$CCLIB"

	# Browse the main configuration file.

	__thpath_library_path_load_file "/etc/ld.so.conf"
}

# add_ld_path(dir[, priority])
#
#   Add a directory to the $PATH.

add_ld_path() {
	__thpath_add_path "${FUNCNAME[0]}${funcstack[1]}" "LIBRARY_PATH" "$@"
	local retcode=$?
	export LD_LIBRARY_PATH="$LIBRARY_PATH"
	return $retcode
}

# del_ld_path(dir)
#
#   Remove a directory from the $PATH.

del_ld_path() {
	__thpath_del_path "${FUNCNAME[0]}${funcstack[1]}" "LIBRARY_PATH" "$@"
	local retcode=$?
	export LD_LIBRARY_PATH="$LIBRARY_PATH"
	return $retcode
}

# print_ld_path()
#
#   Print the $PATH.

print_ld_path() {
	__thpath_print_path "${FUNCNAME[0]}${funcstack[1]}" "LIBRARY_PATH" "$@"
}

# ---
# Manage the $MANPATH
# ---
# __thpath_init_MANPATH()
#
#   Initialize the $MANPATH with the raw path available somewhere.

__thpath_init_MANPATH() {
	local IFS=':'
	local i=''

	if [ ! -z "$MANPATH" ]; then
		__thpath_add_paths "MANPATH" "$MANPATH"
	elif command -v manpath 1>/dev/null 2>/dev/null; then
		__thpath_add_paths "MANPATH" "$(__thpath_manpath -q)"
	fi
}

# add_man_path(dir[, priority])
#
#   Add a directory to the $MANPATH.

add_man_path() {
	__thpath_add_path "${FUNCNAME[0]}${funcstack[1]}" "MANPATH" "$@"
}

# del_man_path(dir)
#
#   Remove a directory from the $MANPATH.

del_man_path() {
	__thpath_del_path "${FUNCNAME[0]}${funcstack[1]}" "MANPATH" "$@"
}

# print_man_path()
#
#   Print the $MANPATH.

print_man_path() {
	__thpath_print_path "${FUNCNAME[0]}${funcstack[1]}" "MANPATH" "$@"
}

# ---
# Manage the $C_INCLUDE_PATH
# ---
# __thpath_init_C_INCLUDE_PATH()
#
#   Initialize the $C_INCLUDE_PATH with the raw path available somewhere.

__thpath_init_C_INCLUDE_PATH() {
	local IFS=':'
	local i=''

	__thpath_add_paths "C_INCLUDE_PATH" "$C_INCLUDE_PATH"
}

# add_include_path(dir[, priority])
#
#   Add a directory to the $C_INCLUDE_PATH.

add_include_path() {
	__thpath_add_path "${FUNCNAME[0]}${funcstack[1]}" "C_INCLUDE_PATH" "$@"
}

# del_include_path(dir)
#
#   Remove a directory from the $C_INCLUDE_PATH.

del_include_path() {
	__thpath_del_path "${FUNCNAME[0]}${funcstack[1]}" "C_INCLUDE_PATH" "$@"
}

# print_include_path()
#
#   Print the $C_INCLUDE_PATH.

print_include_path() {
	__thpath_print_path "${FUNCNAME[0]}${funcstack[1]}" "C_INCLUDE_PATH" "$@"
}

# ---
# Manage the $PKG_CONFIG_PATH
# ---
# __thpath_init_PKG_CONFIG_PATH()
#
#   Initialize the $PKG_CONFIG_PATH with the raw path available somewhere.

__thpath_init_PKG_CONFIG_PATH() {
	local IFS=':'
	local i=''
	local pp=''

	if [ ! -z "$PKG_CONFIG_PATH" ]; then
		__thpath_add_paths "PKG_CONFIG_PATH" "$PKG_CONFIG_PATH"
	elif command -v pkg-config 1>/dev/null 2>/dev/null; then
		__thpath_add_paths "PKG_CONFIG_PATH" \
			"$(pkg-config --variable pc_path pkg-config 2>/dev/null)"
	fi
}

# add_pkg_config_path(dir[, priority])
#
#   Add a directory to the $PKG_CONFIG_PATH.

add_pkg_config_path() {
	__thpath_add_path "${FUNCNAME[0]}${funcstack[1]}" "PKG_CONFIG_PATH" "$@"
}

# del_pkg_config_path(dir)
#
#   Remove a directory from the $PKG_CONFIG_PATH.

del_pkg_config_path() {
	__thpath_del_path "${FUNCNAME[0]}${funcstack[1]}" "PKG_CONFIG_PATH" "$@"
}

# print_pkg_config_path()
#
#   Print the $PKG_CONFIG_PATH.

print_pkg_config_path() {
	__thpath_print_path "${FUNCNAME[0]}${funcstack[1]}" "PKG_CONFIG_PATH" "$@"
}

# ---
# General utilities.
# ---

# add_env(folder[, priority])
#
#   Add an environment, constiting of several paths.

add_env() {
	local dr=''
	local pr=''

	silent=
	if [ x"$1" = x"-s" ]; then
		silent=yes
		shift
	fi

	# Read the directory.

	if [[ -z "$1" || x"$1" == x"-h" \
	   || x"$1" == x"--help" ]]; then
		echo "usage: add_env <path> [priority]" >&2
		return 0
	fi

	dr="$(__thpath_realpath -m -s "$1")"

	# Read the priority.

	pr="$2"

	if __thpath_colonful "$pr"; then
		echo "error: priority should not contain colon" >&2
		return 1
	fi

	# Read the folders!

	for root in "$dr" "$dr/usr" "$dr/usr/local"; do
		add_path "$root/bin" "$pr" \
			&& [ ! "$silent" ] \
			&& echo "'$root/bin' added to \$PATH."
		add_path "$root/sbin" "$pr" \
			&& [ ! "$silent" ] \
			&& echo "'$root/sbin' added to \$PATH."
		add_path "$root/games" "$pr" \
			&& [ ! "$silent" ] \
			&& echo "'$root/games' added to \$PATH."
		add_man_path "$root/share/man" "$pr" \
			&& [ ! "$silent" ] \
			&& echo "'$root/share/man' added to \$MANPATH."
		add_pkg_config_path "$root/lib/pkgconfig" "$pr" \
			&& [ ! "$silent" ] \
			&& echo "'$root/lib/pkgconfig' added to \$PKG_CONFIG_PATH."
		add_ld_path "$root/lib" "$pr" \
			&& [ ! "$silent" ] \
			&& echo "'$root/lib' added to \$LD_LIBRARY_PATH."
		add_include_path "$root/include" "$pr" \
			&& [ ! "$silent" ] \
			&& echo "'$root/include' added to \$C_INCLUDE_PATH."
	done
}

# md(…)
#
#   Alias for `mkdir -p`, with options supported.

alias md='mkdir -p'

# mdcd <folder>
#
#   Create a directory then `cd` into it.

mdcd() {
	local help=''
	local dir=''

	if [ "$#" -lt 1 ]; then
		:
	elif [ x"$1" = x'--' ]; then
		if [ "$#" -eq 2 ]; then
			dir="$2"
		fi
	elif [ "$#" -eq 1 ]; then
		dir="$1"
	fi

	if [ -z "$dir" ]; then
		echo "usage: mdcd <dir>" >&2
		return
	fi

	# Check if something with that name exists.

	if [ -e "$dir" ]; then
		echo "mdcd: $1: file exists" >&2
		return
	fi

	mkdir -p -- "$dir" && cd -- "$dir"
}

# rmwd [-f]
#
#	Remove the current directory and move into the parent directory,
#	while asking for an interactive confirmation.

rmwd() {
	local adir
	local confirm=y
	local answer
	local print_help=

	if [ "$1" = "-f" ]; then
		confirm=
		if [ "$#" -gt 1 ]; then
			print_help=y
		fi
	elif [ "$#" -gt 0 ]; then
		print_help=y
	fi

	if [ ! -z "$print_help" ]; then
		echo "usage: rmwd [-f]" >&2
		return
	fi

	adir="$(pwd)"
	if [ "$confirm" ]; then
		echo -n "Are you sure you want to delete? [y/N] " >&2
		read -r answer

		if ! [ x"$answer" = x"y" ]; then
			echo "Operation cancelled." >&2
			return
		fi
	fi

	cd ..
	rm -rf -- "$adir"
}

# End of file.
