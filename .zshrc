#!/bin/zsh
#******************************************************************************
# .zshrc -- run commands at ZSH startup.
#
# This file is loaded after the zshenv files.
# Read more: http://zsh.sourceforge.net/Doc/Release/Files.html
#******************************************************************************
# Run asdf setup if present.

_load_scripts /opt/asdf-vm/asdf.sh

# Run the local startup script(s) if any.
# You won't find this file in the git repository, as it is
# more or less unique to each machine and set of needs.

_load_scripts "$HOME/.zshrc-local" "$HOME/.zshrc.local" \
	"$HOME/.zshrc.d" "$HOME/.zsh-local" "$HOME/.zsh.d"

# End of file.
